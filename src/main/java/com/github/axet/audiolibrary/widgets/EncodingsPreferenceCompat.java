package com.github.axet.audiolibrary.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.media.AudioRecord;
import android.os.Build;
import android.support.v7.preference.ListPreference;
import android.util.AttributeSet;

import com.github.axet.audiolibrary.R;
import com.github.axet.audiolibrary.app.MainApplication;
import com.github.axet.audiolibrary.app.RawSamples;
import com.github.axet.audiolibrary.app.Sound;
import com.github.axet.audiolibrary.encoders.Factory;
import com.github.axet.audiolibrary.encoders.Format3GP;
import com.github.axet.audiolibrary.encoders.FormatAMR;
import com.github.axet.audiolibrary.encoders.FormatFLAC;
import com.github.axet.audiolibrary.encoders.FormatM4A;
import com.github.axet.audiolibrary.encoders.FormatMKA_AAC;
import com.github.axet.audiolibrary.encoders.FormatMP3;
import com.github.axet.audiolibrary.encoders.FormatOGG;
import com.github.axet.audiolibrary.encoders.FormatOPUS;
import com.github.axet.audiolibrary.encoders.FormatOPUS_MKA;
import com.github.axet.audiolibrary.encoders.FormatOPUS_OGG;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.TreeMap;

public class EncodingsPreferenceCompat extends ListPreference {

    public EncodingsPreferenceCompat(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public EncodingsPreferenceCompat(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public EncodingsPreferenceCompat(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EncodingsPreferenceCompat(Context context) {
        super(context);
    }

    @Override
    public boolean callChangeListener(Object newValue) {
        update(newValue);
        return super.callChangeListener(newValue);
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        Object def = super.onGetDefaultValue(a, index);
        update(def);
        return def;
    }

    public LinkedHashMap<String, String> getSources() {
        CharSequence[] text = getEntries();
        CharSequence[] values = getEntryValues();
        LinkedHashMap<String, String> mm = new LinkedHashMap<>();
        for (int i = 0; i < values.length; i++) {
            String v = values[i].toString();
            String t = text[i].toString();
            mm.put(v, t);
        }
        for (int i = 0; i < Factory.ENCODERS.length; i++) {
            String v = Factory.ENCODERS[i];
            String t = "." + v;
            if (mm.containsKey(v))
                continue;
            mm.put(v, t);
        }
        return mm;
    }

    public boolean isEncoderSupported(String v) {
        return Factory.isEncoderSupported(getContext(), v);
    }

    @Override
    protected void onSetInitialValue(boolean restoreValue, Object defaultValue) {
        super.onSetInitialValue(restoreValue, defaultValue);
        LinkedHashMap<String, String> mm = getSources();
        mm = filter(mm);
        setValues(mm);
    }

    public void setValues(LinkedHashMap<String, String> mm) {
        String v = getValue();
        if (mm.size() > 1) {
            setEntryValues(mm.keySet().toArray(new CharSequence[0]));
            setEntries(mm.values().toArray(new CharSequence[0]));
            int i = findIndexOfValue(v);
            if (i == -1)
                setValueIndex(0);
            else
                setValueIndex(i);
        } else {
            setVisible(false);
        }
        update(v); // defaultValue null after defaults set
    }

    public LinkedHashMap<String, String> filter(LinkedHashMap<String, String> mm) {
        LinkedHashMap<String, String> map = new LinkedHashMap<>();
        for (String v : mm.keySet()) {
            String t = mm.get(v);
            if (!isEncoderSupported(v))
                continue;
            map.put(v, t);
        }
        return map;
    }

    public void update(Object value) {
        String v = (String) value;
        setSummary("." + v);
    }
}
